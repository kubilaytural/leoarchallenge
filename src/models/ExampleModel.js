export default function ExampleUserModel({
  id, profileImage, email, personName, surname,
}) {
  return {
    id,
    avatar: profileImage,
    email,
    name: personName,
    surname,
  };
}
