
import User from './User';

export default function Chat({
  id, text, timestamp, user,
}) {
  return {
    ...User(user),
    message: {
      id,
      text,
      timestamp: timestamp * 1000,
    },
  };
}
