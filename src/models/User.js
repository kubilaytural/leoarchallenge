
export default function User({
  id, name, avatarUrl,
}) {
  return {
    userId: id,
    userName: name,
    avatar: avatarUrl,
  };
}
