import ExampleModel from './ExampleModel';
import Chat from './Chat';
import User from './User';

module.exports = {
  ExampleModel,
  Chat,
  User,
};
