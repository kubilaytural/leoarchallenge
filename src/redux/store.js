import { AsyncStorage } from 'react-native';
import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore, autoRehydrate } from 'redux-persist';
import { axios } from 'utils';
import reducers from './reducers';
import sagas from './sagas';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducers, applyMiddleware(sagaMiddleware), autoRehydrate());

sagaMiddleware.run(sagas);

function onRehydrate() {
  // Uygulama açıldığında geçmiş statein yüklendiği yer
  const currentState = store.getState();
  axios.setToken(currentState.auth.accessToken); // Update Axios Instance's Token
}

persistStore(store, { storage: AsyncStorage }, onRehydrate);

// Hot reloading fix for redux
if (module.hot) {
  module.hot.accept(() => {
    store.replaceReducer(reducers);
  });
}

export default store;
