// ACTION TYPES
export const REQUEST_LOGIN = 'auth/REQUEST_LOGIN';
export const LOGIN_SUCCESS = 'auth/LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'auth/LOGIN_FAILURE';
export const LOGOUT = 'auth/LOGOUT';

const initialState = {
  isLogged: false,
  countryCode: null,
  phoneNumber: null,
  accessToken: null,
  refreshToken: null,
};

// REDUCER
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      const {
        countryCode, phoneNumber, accessToken, refreshToken,
      } = action;
      return {
        ...state,
        isLogged: true,
        countryCode,
        phoneNumber,
        accessToken,
        refreshToken,
      };
    }

    case REQUEST_LOGIN:
    case LOGIN_FAILURE: {
      // SAGA THINGS -> GO API -> SHOW/HIDE LOADING
      return state;
    }

    case LOGOUT: {
      return initialState;
    }

    default: return state;
  }
}

// ACTION CREATORS
export function requestLogin(countryCode, phoneNumber, password) {
  return {
    type: REQUEST_LOGIN,
    countryCode,
    phoneNumber,
    password,
  };
}

export function loginSuccess(countryCode, phoneNumber, accessToken, refreshToken) {
  return {
    type: LOGIN_SUCCESS,
    countryCode,
    phoneNumber,
    accessToken,
    refreshToken,
  };
}

export function loginFailure(error) {
  return {
    type: LOGIN_FAILURE,
    error,
  };
}

export function logout() {
  return {
    type: LOGOUT,
  };
}
