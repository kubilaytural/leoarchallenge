import auth from './auth';
import myChats from './myChats';

module.exports = {
  auth,
  myChats,
};
