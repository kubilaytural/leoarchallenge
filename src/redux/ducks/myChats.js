// ACTION TYPES
export const REQUEST_CHAT_HISTORY = 'myChat/REQUEST_CHAT_HISTORY';
export const SEND_MESSAGE = 'myChat/SEND_MESSAGE';
export const SEND_MESSAGE_SUCCESS = 'myChat/SEND_MESSAGE_SUCCESS';

const initialState = {};

// REDUCER
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case REQUEST_CHAT_HISTORY: {
      return {
        ...state,
      };
    }
    case SEND_MESSAGE: {
      return {
        ...state,
      };
    }
    case SEND_MESSAGE_SUCCESS: {
      const { data } = action;

      return {
        ...state,
        data,
      };
    }

    default: return state;
  }
}

// ACTION CREATORS
export function requestChatHistory() {
  return {
    type: REQUEST_CHAT_HISTORY,
  };
}
export function sendMessage(chattedUserId, ownerUserId, message, chatHistory) {
  return {
    type: SEND_MESSAGE,
    chattedUserId,
    ownerUserId,
    message,
    chatHistory,
  };
}

export function sendMessageSuccess(data) {
  return {
    type: SEND_MESSAGE_SUCCESS,
    data,
  };
}
