import { fork, all } from 'redux-saga/effects';
import { login, logout } from './auth';
import { requestChatHistory, sendMessage } from './myChats';

export default function* root() {
  yield all([
    fork(login),
    fork(logout),
    fork(requestChatHistory),
    fork(sendMessage),
  ]);
}
