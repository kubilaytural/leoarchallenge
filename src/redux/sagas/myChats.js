import { take, call, put } from 'redux-saga/effects';
import {
  REQUEST_CHAT_HISTORY, SEND_MESSAGE, sendMessageSuccess,
} from 'ducks/myChats';
import { chat } from 'services';

export function* requestChatHistory() {
  while (true) {
    try {
      yield take(REQUEST_CHAT_HISTORY); // Take every action

      const chatHistory = yield call(chat.getChatHistory); // Send request
      
      yield put(sendMessageSuccess(chatHistory));
    } catch (error) {
      console.log('getChatHistory ___ error', error);
    }
  }
}


export function* sendMessage() {
  while (true) {
    try {
      const {
        chattedUserId, ownerUserId, message, chatHistory,
      } = yield take(SEND_MESSAGE); // Take every action

      const timestamp = Math.floor(Date.now());
      for (let i = 0; i < chatHistory.data.length - 1; i += 1) {
        if (chatHistory.data[i][0].userId === chattedUserId) {
          const messageItem = {
            userId: ownerUserId,
            userName: 'me',
            avatar: 'https://placebeard.it/640x360',
            message: {
              id: chatHistory.data[i].length,
              text: message,
              timestamp,
            },
          };
          chatHistory.data[i].push(messageItem);
        }
      }

      // Dispatch Success Action with tokens
      yield put(sendMessageSuccess(chatHistory.data));
    } catch (errorMessage) {
      console.log('sendMessage || errorMessage', errorMessage);
    }
  }
}
