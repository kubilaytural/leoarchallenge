import { AsyncStorage } from 'react-native';
import { take, call, put } from 'redux-saga/effects';
import { reduxExample } from 'services';
import { axios } from 'utils';
import {
  REQUEST_LOGIN, LOGOUT, loginSuccess, loginFailure,
} from 'ducks/auth';

export function* login() {
  while (true) {
    try {
      const { phoneNumber, password } = yield take(REQUEST_LOGIN); // Take every action
      const pushToken = yield AsyncStorage.getItem('pushToken');
      const { data: loginResponse } = yield call(reduxExample.auth, phoneNumber, password, pushToken); // Send request

      // Dispatch Success Action with tokens
      yield put(loginSuccess(loginResponse.countryCode, loginResponse.phoneNumber, loginResponse.accessToken, loginResponse.refreshToken));

      // Update Axios Instance's Token
      axios.setToken(loginResponse.accessToken);
    } catch (errorMessage) {
      console.log('errorMessage', errorMessage);
      // Show info
      // alert.showMessage('error', 'login_error');

      yield put(loginFailure(errorMessage));
    }
  }
}

export function* logout() {
  while (true) {
    try {
      const action = yield take(LOGOUT); // Take every action
      // Update Axios Instance's Token
      axios.setToken(null);
    } catch (errorMessage) {
      console.log('errorMessage', errorMessage);
    }
  }
}
