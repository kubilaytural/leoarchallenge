import DeviceInfo from 'react-native-device-info';

function getAppVersion() {
  try {
    const version = DeviceInfo.getReadableVersion().split('.');

    let deviceSystemName = 'undefined_systemName'; //  Example = iOS || Android
    let deviceModel = 'undefined_model'; // Example = iphone 6 || GeneralMobile GM6
    let deviceSystemVersion = 'undefined_systemVersion'; // Example = 11.3 || 24
    let appVersion = 'undefined_appVersion'; // Example = 1.0.0(1)

    deviceSystemName = DeviceInfo.getSystemName();
    deviceModel = DeviceInfo.getModel();
    deviceSystemVersion = DeviceInfo.getSystemVersion();
    appVersion = version[0] + '.' + version[1] + '.' + version[2] + '(' + version[3] + ')'; // EXAMPLE 2.0.1(34)

    const result = deviceSystemName + '_' + appVersion + '_' + deviceSystemVersion + '_' + deviceModel;
    return result;
  } catch (error) {
    console.log('ERROR LOG - getAppVersion() functional error');
  }
}

export default {
  getAppVersion,
};
