import { AsyncStorage } from 'react-native';

// Width Percentage
async function getUserList() {
  const userList = await AsyncStorage.getItem('user_list');
  return JSON.parse(userList);
}

function setUserList(userList) {
  return AsyncStorage.setItem('user_list', JSON.stringify(userList));
}

async function getCurrentUser() {
  const userList = await AsyncStorage.getItem('current_user');
  return JSON.parse(userList);
}

function setCurrentUser(currentUser) {
  return AsyncStorage.setItem('current_user', JSON.stringify(currentUser));
}

async function addUser(ownerUsername) {
  const lowerUserName = ownerUsername.toLowerCase();

  const userList = await getUserList();
  if (userList) {
    const isCurrentUser = userList.filter(item => item.ownerUsername === lowerUserName);
    if (isCurrentUser.length) {
      setCurrentUser(isCurrentUser[0]);
    } else {
      const newUser = { ownerUsername: lowerUserName, ownerUserId: userList[userList.length - 1].ownerUserId + 1 };
      userList.push(newUser);
      setCurrentUser(newUser);
      setUserList(userList);
    }
  } else {
    // Null User Table Case
    const newUser = { ownerUsername: lowerUserName, ownerUserId: 2019 };
    const newUserList = [newUser];
    setCurrentUser(newUser);
    setUserList(newUserList);
  }
}

export default {
  addUser,
  getCurrentUser,
  setCurrentUser,
};
