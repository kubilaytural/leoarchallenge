import { Alert } from 'react-native';
import { i18n } from 'resources';

// Width Percentage
function show(title, message, interpolation = {}, onPress = () => {}) {
  Alert.alert(
    i18n.t(title),
    i18n.t(message, interpolation),
    [{ text: i18n.t('ok'), onPress }],
    { cancelable: false },
  );
}

function showMessage(title, message,  onPress = () => {}) {
  Alert.alert(
    i18n.t(title),
    message,
    [{ text: i18n.t('ok'), onPress }],
    { cancelable: false },
  );
}

export default {
  show,
  showMessage,
};
