// Main Handler
export default function serviceError(error) {
    const { code, response } = error;
  
    if (code) {
      // If network or device based error occurs
      return 'handleServiceErrorByCode';
    } else if (response) {
      // If server responds
      return 'handleServiceErrorByStatusCode';
    }
  
    // Just in case...
    return Promise.reject(error);
  }
  