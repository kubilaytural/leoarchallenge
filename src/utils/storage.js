import { AsyncStorage } from 'react-native';
import I18n from 'react-native-i18n';
import localizationEN from 'moment/locale/en-ca';
import localizationTR from 'moment/locale/tr';
import moment from 'moment';

// Width Percentage
function getAccessToken() {
  return AsyncStorage.getItem('access_token');
}

function setAccessToken(token) {
  return AsyncStorage.setItem('access_token', token);
}

function getRefreshToken() {
  return AsyncStorage.getItem('refresh_token');
}

function setRefreshToken(token) {
  return AsyncStorage.setItem('refresh_token', token);
}

function getLanguage() {
  return AsyncStorage.getItem('language');
}

function getHistoryApiLastUpdate() {
  return AsyncStorage.getItem('history_api_last_update');
}

function setHistoryApiLastUpdate(lastUpdate) {
  return AsyncStorage.setItem('history_api_last_update', lastUpdate.toString());
}

function setLanguage(language) {
  I18n.locale = language;
  switch (language) {
    case 'en':
      moment.updateLocale(language, localizationEN);
      break;
    case 'tr':
      moment.updateLocale(language, localizationTR);
      break;
    default:
      break;
  }

  return AsyncStorage.setItem('language', language);
}

export default {
  getAccessToken,
  setAccessToken,
  getRefreshToken,
  setRefreshToken,
  getLanguage,
  setLanguage,
  getHistoryApiLastUpdate,
  setHistoryApiLastUpdate,
};
