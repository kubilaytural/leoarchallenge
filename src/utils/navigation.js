import { Navigation } from 'react-native-navigation';
import { i18n, images } from 'resources';

const didAppearListeners = {};
const navigationButtonListeners = {};


// Utilities
Navigation.isScreenVisible = componentId => Navigation.store.getRefForId(componentId) != undefined;
Navigation.setTitle = title => ({ topBar: { leftButtons: [], title: { text: i18n.t(title) }, rightButtons: [] } });
Navigation.setCloseButton = () => ({ topBar: { leftButtons: [{ id: 'close', icon: images.ic_close }] } });
Navigation.setRightButton = (id, icon) => ({ topBar: { rightButtons: [{ id, icon }] } });
Navigation.noHeader = () => ({ topBar: { visible: false, animate: true, drawBehind: true } });
Navigation.hideBottomTabs = () => ({ bottomTabs: { drawBehind: true } });

Navigation.showOverlayWithName = (name, props = {}) => Navigation.showOverlay({ component: { name, passProps: props } });
Navigation.showModalWithName = (name, props = {}) => Navigation.showModal({ component: { name, passProps: props } });
Navigation.showModalWithStack = (name, props = {}) => Navigation.showModal({ stack: { children: [{ component: { name, passProps: props } }] } });
Navigation.pushWithName = (componentId, name, props = {}) => Navigation.push(componentId, { component: { name, passProps: props } });

Navigation.registerDidAppearListener = (componentId, listener) => { didAppearListeners[componentId] = listener; };
Navigation.registerNavigationButtonListener = (componentId, listener) => { navigationButtonListeners[componentId] = listener; };

/* ********** LISTENERS ********** */
function didAppear(componentId, componentName) {
  // Call the listener if exist
  if (didAppearListeners[componentId]) didAppearListeners[componentId]();
}
function didDisappear(componentId, componentName) {}


Navigation.events().registerComponentDidAppearListener(didAppear);
Navigation.events().registerComponentDidDisappearListener(didDisappear);

export default Navigation;
