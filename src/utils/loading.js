import navigation from './navigation';

export default {
  show: async () => {
    navigation.showOverlay({
      component: {
        id: 'LoadingModal',
        name: 'LoadingModal',
      },
    });
  },

  hide: () => {
    navigation.dismissOverlay('LoadingModal');
  },
};
