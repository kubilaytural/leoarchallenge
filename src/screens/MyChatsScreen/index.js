import React, { PureComponent } from 'react';
import { View, FlatList } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Text } from 'components';
import { i18n, images } from 'resources';
import { navigation, userHelper } from 'utils';
import flows from 'flows';
import { ChatRowItem } from './components';
import styles from './styles';

class MyChatsScreen extends PureComponent {
  static options() {
    const rightButtons = [{ id: 'logout', icon: images.ic_logout }];
    const title = { text: i18n.t('chats') };

    return { topBar: { rightButtons, title } };
  }

  constructor(props) {
    super(props);

    this.state = {
      ownerUsername: '',
    };
  
    // Functions
    this.renderItem = this.renderItem.bind(this);

    navigation.events().bindComponent(this);
  }
  
  componentDidMount() {
    this.getCurrentUser();
  }

  async getCurrentUser() {
    try {
      const { ownerUsername } = await userHelper.getCurrentUser();
      this.setState({ ownerUsername });
    } catch (error) {
      console.log('error', error);
    }
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === 'logout') flows.logout();
  }

  renderItem = ({ item }) => (
    <ChatRowItem componentId={this.props.componentId} data={item} />
  )

  render() {
    const { myChats } = this.props;
    const { ownerUsername } = this.state;
    const myChatsArray = Object.keys(myChats).map(i => myChats[i]);

    return (
      <View style={styles.container}>
        <FlatList
          keyExtractor={this.keyExtractor}
          data={myChatsArray[0]}
          extraData={this.props}
          renderItem={this.renderItem}
        />
        <Text
          translation="currentUserName"
          translationData={{ name: ownerUsername }}
          size={16}
          weight="BOLD"
          style={styles.currentUserText}
        />
      </View>
    );
  }
}

MyChatsScreen.propTypes = {
  myChats: PropTypes.any,
};
MyChatsScreen.defaultProps = {
  myChats: [],
};

const mapStateToProps = state => ({
  myChats: state.myChats,
});

const mapDispatchToProps = () => ({});


export default connect(mapStateToProps, mapDispatchToProps)(MyChatsScreen);
