
import React, { PureComponent } from 'react';
import { View } from 'react-native';
import moment from 'moment';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, Image } from 'components';
import { navigation } from 'utils';
import styles from './styles';

class ChatRowItem extends PureComponent {
  constructor(props) {
    super(props);

    // Functions
    this.onChatRow = this.onChatRow.bind(this);
  }

  onChatRow() {
    const { componentId, data } = this.props;
    navigation.pushWithName(componentId, 'ChatScreen', { chattedUserId: data[0].userId, chattedUsername: data[0].userName });
  }

  render() {
    const { data } = this.props;
    const { avatar, userName } = data[0];
    const lastMessage = data[data.length - 1].message.text;
    const lastMessageTime = data[data.length - 1].message.timestamp;
    const formatedLastMessageTime = moment(lastMessageTime).format('HH:mm');

    return (
      <TouchableOpacity style={styles.container} onPress={this.onChatRow}>
        <Image fast uri={avatar} style={styles.avatar} />
        <View style={styles.middleContainer}>
          <Text size={16} weight="SEMIBOLD" style={styles.text}>{userName}</Text>
          <Text size={13} weight="MEDIUM" color="action_sheet_title_color" style={styles.lastMessageText}>{lastMessage}</Text>
        </View>
        <View>
          <Text size={13} weight="MEDIUM" color="action_sheet_title_color" style={styles.lastMessageText}>{formatedLastMessageTime}</Text>
        </View>
        <View style={styles.rightIconContainer}>
          <Image source="ic_right_arrow" style={styles.rightIcon} />
        </View>
      </TouchableOpacity>
    );
  }
}

ChatRowItem.propTypes = {
  componentId: PropTypes.string,
  data: PropTypes.any,
};
ChatRowItem.defaultProps = {
  componentId: '',
  data: [],
};

export default ChatRowItem;
