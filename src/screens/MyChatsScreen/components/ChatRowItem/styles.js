import { StyleSheet } from 'react-native';
import { colors } from 'resources';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 10,
    marginVertical: 5,
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderColor: colors.card_icon_color,
  },
  middleContainer: { flex: 1, marginLeft: 10 },
  timeText: { marginTop: 10 },
  rightIconContainer: { justifyContent: 'center' },
  avatar: {
    height: 60,
    width: 60,
    borderRadius: 30,
  },
  lastMessageText: { marginTop: 5 },
  rightIcon: {
    height: 12,
    width: 15,
  },
});

export default styles;
