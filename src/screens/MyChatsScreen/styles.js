import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  button: {
    marginVertical: 20,
  },
  text: {
    marginVertical: 10,
  },
  currentUserText: {
    textAlign: 'center',
    paddingBottom: 20,
  },
});

export default styles;
