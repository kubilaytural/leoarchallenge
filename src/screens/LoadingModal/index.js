import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';
import styles from './styles';

class LoadingModal extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator color="white" size="large" />
      </View>
    );
  }
}

export default LoadingModal;
