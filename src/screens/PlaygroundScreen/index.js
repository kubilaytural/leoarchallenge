import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Container, TouchableOpacity, Text, Image, Button,
} from 'components';
import { navigation, alert } from 'utils';
import styles from './styles';

function onAlertMe() {
  alert.show('dummyAlertTitle', 'dummyAlertDescription');
}

class PlaygroundScreen extends Component {
  static get options() { return navigation.setTitle('home'); }

  onReduxTest() {
    navigation.pushWithName(this.props.componentId, 'LoginScreen', '');
  }

  render() {
    return (
      <Container style={styles.container}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <TouchableOpacity style={{ alignItems: 'center' }} onPress={onAlertMe}>
            <Image source="img_starterIcon" style={styles.image} />
            <Text translation="appName" size={22} weight="MEDIUM" style={styles.text} />
          </TouchableOpacity>
        </View>
        <View style={{ height: '15%' }}>
          <Button translation="reduxTest" onPress={() => this.onReduxTest()} />
        </View>
      </Container>
    );
  }
}

export default PlaygroundScreen;
