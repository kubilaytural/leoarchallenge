import { StyleSheet } from 'react-native';
import { colors } from 'resources';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
  },
  image: { width: 100, height: 100 },
  text: { marginVertical: 20 },

  button: {
    backgroundColor: colors.secondary,
    alignItems: 'center',
    marginHorizontal: 20,
  },
});

export default styles;
