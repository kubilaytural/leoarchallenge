import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as Animatable from 'react-native-animatable';
import { requestChatHistory } from 'ducks/myChats';
import { Button, TextInput, Text } from 'components';
import { navigation, userHelper, storage } from 'utils';
import { colors } from 'resources';
import flows from 'flows';
import styles from './styles';

class SignInScreen extends PureComponent {
  static get options() { return navigation.setTitle('signIn'); }

  constructor(props) {
    super(props);

    this.state = { inputBorderColor: 'card_icon_color' };

    // Functions
    this.onChangeText = this.onChangeText.bind(this);
    this.onSignIn = this.onSignIn.bind(this);
  }

  onChangeText(text) {
    if (text.length > 2) {
      this.setState({ inputBorderColor: 'card_icon_color' });
    }
  }

  onSignIn() {
    try {
      const username = this.usernameInput.getText();
      if (username.length <= 2) {
        this.setState({ inputBorderColor: 'pinkish_red' });
        this.startAnimation.shake(2000);
        return;
      }
      this.getChatHistory();
      userHelper.addUser(username);
      flows.login('accessToken');
    } catch (error) {
      console.log('error', error);
    }
  }

  async getChatHistory() {
    const lastUpdateDate = await storage.getHistoryApiLastUpdate();
    if (!lastUpdateDate) {
      const { requestChatHistory } = this.props;
      requestChatHistory();
    }
  }

  render() {
    const { inputBorderColor } = this.state;
    return (
      <KeyboardAwareScrollView
        automaticallyAdjustContentInsets={false}
        keyboardShouldPersistTaps={'handled'}
        contentContainerStyle={styles.keyboardContainer}
      >
        <View style={styles.container}>
          <Text translation="username" size={16} weight="SEMIBOLD" style={styles.text} />
          <Animatable.View ref={(refs) => { this.startAnimation = refs; }}>
            <TextInput
              ref={(ref) => { this.usernameInput = ref; }}
              placeholder="username"
              style={{ borderColor: colors[inputBorderColor] }}
              onChangeText={this.onChangeText}
            />
          </Animatable.View>
          <Button translation="login" onPress={this.onSignIn} style={styles.button} />
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const mapStateToProps = state => ({
  myChats: state.myChats,
});

const mapDispatchToProps = dispatch => ({
  requestChatHistory: bindActionCreators(requestChatHistory, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);
