import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  keyboardContainer: { flex: 1 },
  container: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 20,
  },
  button: {
    marginVertical: 20,
  },
  text: {
    marginVertical: 10,
  },
});

export default styles;
