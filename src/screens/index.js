import { Navigation } from 'react-native-navigation';

import LoadingModal from './LoadingModal';
import PlaygroundScreen from './PlaygroundScreen';
import SignInScreen from './SignInScreen';
import MyChatsScreen from './MyChatsScreen';
import ChatScreen from './ChatScreen';

// register all screens of the app (including internal ones)
export default function registerScreens(Provider = null, store = null) {
  Navigation.registerComponent('LoadingModal', () => LoadingModal);
  Navigation.registerComponentWithRedux('PlaygroundScreen', () => PlaygroundScreen, Provider, store);
  Navigation.registerComponentWithRedux('SignInScreen', () => SignInScreen, Provider, store);
  Navigation.registerComponentWithRedux('MyChatsScreen', () => MyChatsScreen, Provider, store);
  Navigation.registerComponentWithRedux('ChatScreen', () => ChatScreen, Provider, store);
}
