import { StyleSheet, Dimensions } from 'react-native';
import { colors } from 'resources';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    maxWidth: width / 1.3,
    minWidth: 70,
    backgroundColor: colors.primary,
    marginLeft: 15,
    marginVertical: 3,
    padding: 10,
    borderRadius: 10,
    shadowColor: '#d9d9d9',
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1,
    },
  },
  timeText: {
    flexDirection: 'row',
    position: 'absolute',
    right: 5,
    bottom: 5,
  },
  timeTextContainer: {
    flexDirection: 'row',
    position: 'absolute',
    right: 5,
    bottom: 5,
    alignItems: 'center',

  },
  rightContainer: { alignItems: 'flex-end' },
  messageText: {
    marginBottom: 8,
  },
  tickImage: {
    height: 10,
    width: 10,
    marginLeft: 3,
  },
});

export default styles;
