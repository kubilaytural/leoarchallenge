
import React, { PureComponent } from 'react';
import { View } from 'react-native';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Text, Image } from 'components';
import { colors } from 'resources';
import { userHelper } from 'utils';
import styles from './styles';

class ChatRowItem extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: {},
    };
  }

  componentDidMount() {
    this.getCurrentUser();
  }

  async getCurrentUser() {
    const currentUser = await userHelper.getCurrentUser();
    this.setState({ currentUser });
  }

  renderMyChatBalloon() {
    const { message: { text, timestamp } } = this.props;

    return (
      <View style={styles.rightContainer}>
        <View style={[styles.container, { backgroundColor: colors.myBalloonBackground, marginRight: 15 }]}>
          <Text size={16} weight="MEDIUM" style={styles.messageText}>{text}</Text>
          <View style={styles.timeTextContainer}>
            <Text size={11} weight="MEDIUM" color="messageTextColor">{moment(timestamp).format('HH:mm')}</Text>
            <Image source="doubleTick" style={styles.tickImage} />
          </View>
        </View>
      </View>
    );
  }

  render() {
    const { userId, message: { text, timestamp } } = this.props;
    const { currentUser: { ownerUserId } } = this.state;
    if (userId === ownerUserId) return this.renderMyChatBalloon(text);
    if (userId !== ownerUserId && userId > 2000) return null;

    return (
      <View style={styles.container}>
        <Text size={16} weight="MEDIUM" style={styles.messageText}>{text}</Text>
        <Text size={11} weight="MEDIUM" color="messageTextColor" style={styles.timeText}>{moment(timestamp).format('HH:mm')}</Text>
      </View>
    );
  }
}

ChatRowItem.propTypes = {
  message: PropTypes.any,
};
ChatRowItem.defaultProps = {
  message: [],
};

export default ChatRowItem;
