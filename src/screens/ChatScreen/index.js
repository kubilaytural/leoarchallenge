import React, { PureComponent } from 'react';
import { View, FlatList } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { TextInput, Image, TouchableOpacity } from 'components';
import { sendMessage } from 'ducks/myChats';
import { userHelper } from 'utils';
import { ChatBalloon } from './components';
import styles from './styles';


function renderItem({ item }) { return <ChatBalloon {...item} />; }

class ChatScreen extends PureComponent {
  static options({ chattedUsername }) {
    const title = { text: chattedUsername };

    return { topBar: { title } };
  }

  constructor(props) {
    super(props);

    this.state = { selectedChatHistory: [] };

    // Functions
    this.onSend = this.onSend.bind(this);
  }

  componentDidMount() {
    this.getSelectedChatHistory();
  }

  async onSend() {
    try {
      const messageText = this.messageInput.getText();
      if (!messageText.length) return; // if not message text -- no action
      const { chattedUserId, sendMessage, myChats } = this.props;
      const { ownerUserId } = await userHelper.getCurrentUser();
      sendMessage(chattedUserId, ownerUserId, messageText, myChats);
      this.messageInput.setText(''); // Clear MessageInput
    } catch (error) {
      console.log('onSend() Error', error);
    }
  }

  getSelectedChatHistory() {
    try {
      const { myChats, chattedUserId } = this.props;
      const selectedChatHistory = myChats.data.filter(item => item[0].userId === chattedUserId);
      this.setState({ selectedChatHistory: selectedChatHistory[0] });
    } catch (error) {
      console.log('error', error);
      alert('Beklenmedik bir hata oluştu!');
    }
  }


  render() {
    const { selectedChatHistory } = this.state;

    return (
      <KeyboardAwareScrollView
        automaticallyAdjustContentInsets={false}
        keyboardShouldPersistTaps={'handled'}
        contentContainerStyle={styles.keyboardContainer}
      >
        <View style={styles.container}>
          <FlatList
            keyExtractor={this.keyExtractor}
            data={selectedChatHistory}
            extraData={this.props}
            renderItem={renderItem}
          />
          <View style={styles.footerContainer}>
            <TextInput
              ref={(ref) => { this.messageInput = ref; }}
              multiline
              style={styles.messageInput}
            />
            <TouchableOpacity style={styles.sendButtonContainer} onPress={this.onSend}>
              <Image source="ic_sendButton" style={styles.sendButton} />
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

ChatScreen.propTypes = {
  myChats: PropTypes.any,
  chattedUserId: PropTypes.number,
};
ChatScreen.defaultProps = {
  myChats: [],
  chattedUserId: 0,
};

const mapStateToProps = state => ({
  myChats: state.myChats,
});

const mapDispatchToProps = dispatch => ({
  sendMessage: bindActionCreators(sendMessage, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);
