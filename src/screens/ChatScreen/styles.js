import { StyleSheet, Dimensions } from 'react-native';
import { colors } from 'resources';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  keyboardContainer: { flex: 1 },
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: colors.chatBackgroundColor,
  },
  footerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.wpGray,
    flexDirection: 'row',
  },
  messageInputContainer: {
    justifyContent: 'center',
  },
  messageInput: {
    maxHeight: 120,
    width: width / 1.2,
    padding: 8,
    borderRadius: 18,
    marginVertical: 8,
    borderWidth: 2,
    textAlign: 'left',
  },
  sendButtonContainer: {
    marginLeft: 3,
    padding: 8,
    borderRadius: 16,
    backgroundColor: colors.wpBlue,
  },
  sendButton: {
    height: 15,
    width: 15,
  },
});

export default styles;
