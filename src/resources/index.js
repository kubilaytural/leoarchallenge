import constants from './constants';
import colors from './colors';
import images from './images';
import fonts from './fonts';
import i18n from './i18n';
import fontTypes from './font_types';
import dimens from './dimens';

module.exports = {
  constants,
  colors,
  images,
  fonts,
  i18n,
  fontTypes,
  dimens,
};
