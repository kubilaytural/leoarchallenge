module.exports = {
  img_starterIcon: require('./images/starter-icon.png'),
  ic_close: require('./images/icons/ic_close.png'),
  ic_back: require('./images/icons/ic_close.png'),
  ic_right_arrow: require('./images/icons/right-arrow.png'),
  doubleTick: require('./images/icons/Shape.png'),
  ic_sendButton: require('./images/icons/send-button.png'),
  ic_logout: require('./images/icons/ic_logout.png'),
};
