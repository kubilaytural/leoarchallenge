import { Platform } from 'react-native';

const fonts = Platform.select({
  ios: {
    primary: {
      LIGHT: 'Montserrat-Light',
      REGULAR: 'Montserrat-Regular',
      MEDIUM: 'Montserrat-Medium',
      SEMIBOLD: 'Montserrat-SemiBold',
      BOLD: 'Montserrat-Bold',
    },
  },
  android: {
    primary: {
      LIGHT: 'Montserrat-Light',
      REGULAR: 'Montserrat-Regular',
      MEDIUM: 'Montserrat-Medium',
      SEMIBOLD: 'Montserrat-SemiBold',
      BOLD: 'Montserrat-Bold',
    },
  },
});

export default fonts;
