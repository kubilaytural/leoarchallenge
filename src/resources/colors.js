export default {
  primary: '#FFF',
  secondary: '#1a8ff7',
  third: '#36394c',
  fourth: '#4c4f62',
  black: '#000000',
  white: '#FFFFFF',
  transparent: 'transparent',
  transparent_black: 'rgba(0, 0, 0, 0.2)',
  background: '#F0F6FF',
  main_text_color: '#464B6D',
  list_icon_color: '#436CD7',
  main_heading_color: '#709CE2',
  widget_prop_color: '#96A4DC',
  secondary_header: '#969AB8',
  action_bg_color: '#F0F6FF',
  page_bg_color: '#FFFFFF',
  pinkish_red: '#F4587E',
  mustard_orange: '#FEBA4F',
  spring_green: '#6CB94C',
  sky_blue: '#75AFFF',
  mystic_violet: '#8E89E8',
  misty_brown: '#B78B9E',
  banana_yellow: '#FFD666',
  fresh_green: '#52C574',
  tooltip_color: '#E0E0E0',
  big_header_color: '#3460D3',
  card_icon_color: '#E6E9FC',
  placeholder_color: '#B8CDED',
  action_sheet_title_color: '#757575',
  action_sheet_tintColor: '#007AFF',
  action_sheet_buttonUnderlayColor: '#F4F4F4',
  messageTextColor: '#BEBEC4',
  chatBackgroundColor: '#EAE5E0',
  myBalloonBackground: '#DCF8C6',
  wpBlue: '#017CFF',
  wpGray: '#F7F7F7',
};
