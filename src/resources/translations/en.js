export default {
  appName: 'Core Kit',
  dummyAlertTitle: 'Core Kit',
  dummyAlertDescription: 'give star at Github!',

  // Base Translations
  ok: 'Okey',
  cancel: 'Cancel',
  yes: 'Yes',
  no: 'No',
  warning: 'Warning',

  login: 'Login',
  isLogin: 'is Login?',
  home: 'Home',
  reduxTest: 'Redux Test',
  signIn: 'Sign In',
  username: 'Username',
  chats: 'Chats',
  currentUserName: 'Current User Name: {{name}}',
};
