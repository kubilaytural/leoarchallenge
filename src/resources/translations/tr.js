export default {
  appName: 'Core Kit',
  dummyAlertTitle: 'Core Kit',
  dummyAlertDescription: 'Github da yıldız ver!',

  // Base Translations
  ok: 'Tamam',
  cancel: 'İptal',
  yes: 'Evet',
  no: 'Hayır',
  warning: 'Uyarı',

  login: 'Giriş Yap',
  isLogin: 'is Login?',
  home: 'Anasayfa',
  reduxTest: 'Redux Test',
  signIn: 'Oturum Aç',
  username: 'Kullanıcı Adı',
  chats: 'Sohbetler',
  currentUserName: 'Mevcut Kullanıcı Adı: {{name}}',
};
