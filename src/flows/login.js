import storage from '../utils/storage';
import { startFromChats } from '../index';

export default async function login(token) {
  await storage.setAccessToken(token);
  // axios.setToken(token);

  startFromChats();
}
