import storage from '../utils/storage';
import { startFromLogin } from '../index';

export default async function userLogout() {
  await storage.setAccessToken('');
  // axios.setToken(null);

  startFromLogin();

//   // Clear redux states
//   store.dispatch(logout());
}
