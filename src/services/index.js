import getExample from './getExample';
import reduxExample from './reduxExample';
import chat from './chat';

module.exports = {
  getExample,
  reduxExample,
  chat,
};
