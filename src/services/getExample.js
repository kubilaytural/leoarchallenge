import { axios } from 'utils';

export default function getExample() {
  const params = { };

  return axios.get('https://jsonplaceholder.typicode.com/users', { params });
}
