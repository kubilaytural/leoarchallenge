import { axios, storage } from 'utils';
import { Chat } from 'models';

const groupBy = function(xs, key) {
  return xs.reduce(function(rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, []);
};

export default async function getChatHistory() {
  try {
    const headers = { 'NO_LOADING': true };
    const params = {};

    const { data } = await axios('https://jsonblob.com/api/jsonBlob/4f421a10-5c4d-11e9-8840-0b16defc864d', { method: 'GET', headers, params });

    const chatHistory = data.map(item => Chat(item));
    const groupedChatHistory = groupBy(chatHistory, 'userId');

    for (let i = 0; i < groupedChatHistory.length; i += 1) {
      groupedChatHistory[i].sort((a, b) => (a.message.timestamp > b.message.timestamp) ? 1 : -1)
    }

    const timestamp = Math.floor(Date.now());
    storage.setHistoryApiLastUpdate(timestamp);
  
    return groupedChatHistory;
  } catch (error) {
    console.log(`getChatHistory Error`, error);
    return error;
  }
}
