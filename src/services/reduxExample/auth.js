export default async function auth(phoneNumber, pin, pushToken) {
  const response = {
    countryCode: 'countryCode',
    phoneNumber: 'phoneNumber',
    accessToken: 'accessToken',
    refreshToken: 'refreshToken',
  };

  return { data: response };
}
