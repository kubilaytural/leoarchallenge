import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import {
  fonts, colors, images,
} from 'resources';
import { storage } from 'utils';
import store from './redux/store';
import registerScreens from './screens';

registerScreens(Provider, store);

export function startFromLogin() {
  Navigation.setRoot({
    root: {
      stack: {
        children: [{
          component: {
            name: 'SignInScreen',
          },
        }],
        options: {
          // backgroundImage: images.login_background,
        },
      },
    },
  });
}

export function startFromChats() {
  Navigation.setRoot({
    root: {
      stack: {
        children: [{
          component: {
            name: 'MyChatsScreen',
          },
        }],
        options: {
          // backgroundImage: images.login_background,
        },
      },
    },
  });
}

Navigation.events().registerAppLaunchedListener(async () => {
  Navigation.setDefaultOptions({
    topBar: {
      visible: true,
      drawBehind: false,
      title: {
        color: colors.white,
        fontSize: 18,
        alignment: 'center',
        fontFamily: fonts.primary.MEDIUM,
      },
      background: {
        color: colors.third,
      },
      backButton: {
        icon: images.icon_back,
        showTitle: false,
        color: 'white',
      },
      buttonColor: 'white',
    },
    layout: {
      backgroundColor: colors.background,
      orientation: ['portrait'],
    },
    orientation: 'portrait',
    screenBackgroundColor: 'white',
  });

  const accessToken = await storage.getAccessToken();

  if (accessToken) {
    startFromChats();
  } else {
    startFromLogin();
  }
});
