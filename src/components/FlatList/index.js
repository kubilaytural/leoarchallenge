import React from 'react';
import { FlatList } from 'react-native';


const MyFlatList = ({ props }) => (
  <FlatList {...props} showsHorizontalScrollIndicator={false} />
);

MyFlatList.propTypes = {};
MyFlatList.defaultProps = {};

export default MyFlatList;
