import React from 'react';
import PropTypes from 'prop-types';
import TouchableOpacity from '../TouchableOpacity';
import Text from '../Text';
import styles from './styles';

const Button = ({
  textColor, weight, translation, style, textSize, onPress,
}) => (
  <TouchableOpacity style={[styles.container, style]} onPress={onPress}>
    <Text
      size={textSize}
      color={textColor}
      weight={weight}
      translation={translation}
      style={styles.text}
    />
  </TouchableOpacity>
);

Button.propTypes = {
  textColor: PropTypes.string,
  weight: PropTypes.string,
  translation: PropTypes.string,
  onPress: PropTypes.func,
  textSize: PropTypes.number,
};
Button.defaultProps = {
  textColor: 'white',
  weight: 'MEDIUM',
  translation: '',
  onPress: () => {},
  textSize: 18,
};

export default Button;
