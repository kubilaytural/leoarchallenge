import { StyleSheet } from 'react-native';
import { colors } from 'resources';

const styles = StyleSheet.create({
  container: {
    padding: 20,
    borderRadius: 40,
    backgroundColor: colors.secondary,
    alignItems: 'center',
  },
});

export default styles;
