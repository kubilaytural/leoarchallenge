import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native';
import FastImage from 'react-native-fast-image';
import { images } from 'resources';

class MyImageComponent extends PureComponent {
  render() {
    const {
      uri, // Image Url
      source, // Local Image
      fast, // FastImage props // (if true return FastImage library)
    } = this.props;
    const mySource = uri ? { uri } : source ? images[source] : null;
    const MyImage = fast ? FastImage : Image;
    return <MyImage {...this.props} source={mySource} />;
  }
}

MyImageComponent.propTypes = {
  fast: PropTypes.bool,
  uri: PropTypes.string,
  source: PropTypes.string,
};

MyImageComponent.defaultProps = {
  fast: false,
  uri: null,
  source: null,
};

export default MyImageComponent;
