import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './styles';

class Container extends PureComponent {
  render() {
    const { scrollView, keyboardAware, style } = this.props;
    const MyView = scrollView ? ScrollView : keyboardAware ? KeyboardAwareScrollView : View;
    return (
      <View style={styles.container}>
        <MyView extraHeight={200} style={[{ flex: 1 }, style]}>
          {this.props.children}
        </MyView>
      </View>
    );
  }
}

Container.propTypes = {
  scrollView: PropTypes.bool,
  keyboardAware: PropTypes.bool,
};

Container.defaultProps = {
  scrollView: false,
  keyboardAware: false,
};

export default Container;
