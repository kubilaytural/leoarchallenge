import { StyleSheet } from 'react-native';
import { colors } from 'resources';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
  },
});

export default styles;
