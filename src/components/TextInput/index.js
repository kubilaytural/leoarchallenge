import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextInput } from 'react-native';
import { i18n, colors } from 'resources';
import styles from './styles';

class MyTextInput extends Component {
  constructor(props) {
    super(props);

    const { color } = this.props;


    this.state = { text: '' };

    this.inlineStyle = { ...styles.input };

    // Props olarak colors gelirse ve lokalde kayıtlı ise color style'a dahil edilir.
    if (color && color in colors) this.inlineStyle.color = colors[color];

    // Functions
    this.onChangeText = this.onChangeText.bind(this);
    this.getText = this.getText.bind(this);
  }

  onChangeText(text) {
    this.setState({ text });
    this.props.onChangeText(text);
  }

  // Component dışı value değerini almak için kullanılır.
  getText() {
    const { text } = this.state;

    // Text null değilse trim edilip gönderilir. Null'sa mevcut değer gönderilir.
    return text ? text.trim() : text;
  }

  // Component dışı value atamak için kullanılır.
  setText(text) { this.setState({ text }); }

  // Component dışı input value'su bu fonksiyon ile temizlenebilir.
  clear() {
    this.props.onChangeText('');
    this.textInput.clear();
  }

  focus() { this.textInput.focus(); }

  render() {
    const {
      placeholder, style, editable, secureTextEntry, keyboardType, maxLength, returnKeyType, onSubmitEditing, multiline
    } = this.props;
    const { text } = this.state;

    const placeholderText = placeholder ? i18n.t(placeholder) : '';

    return (
      <TextInput
        ref={(inputRef) => { this.textInput = inputRef; }}
        underlineColorAndroid="transparent"
        onChangeText={this.onChangeText}
        value={text}
        placeholder={placeholderText}
        placeholderTextColor={colors.placeholder_color}
        accessible
        focusable // accesiblity, only android
        accessibilityLabel={placeholderText}
        editable={editable}
        maxLength={maxLength}
        keyboardType={keyboardType}
        secureTextEntry={secureTextEntry}
        returnKeyType={returnKeyType}
        onSubmitEditing={onSubmitEditing}
        style={[this.inlineStyle, style]}
        multiline={multiline}
      />
    );
  }
}

MyTextInput.propTypes = {
  onChangeText: PropTypes.func,
  placeholder: PropTypes.string,
  style: PropTypes.any,
  color: PropTypes.string,
  editable: PropTypes.bool,
  secureTextEntry: PropTypes.bool,
  keyboardType: PropTypes.string,
  maxLength: PropTypes.number,
  returnKeyType: PropTypes.string,
  onSubmitEditing: PropTypes.func,
  multiline: PropTypes.bool,
};
MyTextInput.defaultProps = {
  onChangeText: () => {},
  placeholder: '',
  style: null,
  color: 'main_text_color',
  editable: true,
  secureTextEntry: false,
  keyboardType: 'default',
  maxLength: 1000,
  returnKeyType: 'done',
  onSubmitEditing: null,
  multiline: false,
};

export default MyTextInput;
