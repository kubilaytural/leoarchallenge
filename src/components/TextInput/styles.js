import { StyleSheet } from 'react-native';
import { fonts, colors } from 'resources';

const styles = StyleSheet.create({
  input: {
    fontSize: 18,
    fontWeight: '600',
    textAlign: 'center',
    backgroundColor: colors.page_bg_color,
    color: colors.main_text_color,
    borderColor: colors.card_icon_color,
    borderWidth: 1,
    padding: 15,
    borderRadius: 16,
    fontFamily: fonts.primary.MEDIUM,
  },
});

export default styles;
