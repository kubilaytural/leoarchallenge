// Common Components
import Button from './Button';
import Text from './Text';
import FlatList from './FlatList';
import Container from './Container';
import Image from './Image';
import TouchableOpacity from './TouchableOpacity';
import TextInput from './TextInput';

module.exports = {
  // Common Components
  Button,
  Text,
  FlatList,
  Container,
  Image,
  TouchableOpacity,
  TextInput,
};
