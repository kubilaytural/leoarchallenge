package com.corekit;

import android.content.Intent;
import com.facebook.react.modules.core.PermissionListener;
import com.reactnativenavigation.NavigationActivity;

public class MainActivity extends NavigationActivity {
    private PermissionListener listener; // <- add this attribute

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        if (listener != null)
        {
            listener.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}